import React, {useState} from 'react';
import './style.css';

const SignIn = () => {
  const [loginValid, setLoginValid] = useState(false);
  const [passwordValid, setPasswordValid] = useState(false);
  const [disabledButton, setDisabledButton] = useState(true);

  const onChangeLogin = (event) => {
    if (event.target.value.length > 6) {
      setLoginValid(true);
    } else {
      setLoginValid(false);
    }
    if (loginValid && passwordValid) {
      setDisabledButton(false);
    } else {
      setDisabledButton(true);
    }
  };

  const onChangePassword = (event) => {
    if (event.target.value.length > 6) {
      setPasswordValid(true);
    } else {
      setPasswordValid(false);
    }
    if (loginValid && passwordValid) {
      setDisabledButton(false);
    } else {
      setDisabledButton(true);
    }
  };

  const onClickButton = () => {
    let inputs = document.querySelectorAll('input');
    let login = inputs[1].value;
    let password = inputs[2].value;
    console.log(`Логин: ${login} пароль: ${password}`);
  };

  return (
    <div>
      <form className="authorization_form form">
        {loginValid ? <div></div> : <div className="wrong_password_notification"><p>Неверный формат логина</p></div>}
        <input className="form_username" type="email" placeholder="Email" onChange={onChangeLogin}></input>
        <input className="form_password" type="password" placeholder="Password" onChange={onChangePassword}></input>
        <input className="form_button" type="button" value="Войти" disabled={disabledButton}
               onClick={onClickButton}></input>
      </form>
    </div>
  );
};

export default SignIn;