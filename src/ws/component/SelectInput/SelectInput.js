import React, {useState} from 'react';
import PropTypes from "prop-types";
import './style.css';

const SelectInput = (props) => {

  const [currentValue, setCurrentValue] = useState('');

  const options = props.colors.map(item => (
    <option id={item.id} value={item.value}>{item.name}</option>
  ));

  const onChangeSelect = (event) => {
    setCurrentValue(event.target.value);
  };

  const onClickButton = () => {
    document.body.style.backgroundColor = currentValue;
  };

  return (
    <div className="select_block">
      <form>
        <label> Цвета:
          <select value={currentValue} className="select" onChange={onChangeSelect}>
            {options}
          </select>
        </label>
        <input type="button" value="Поменять цвет" onClick={onClickButton}></input>
      </form>
    </div>
  );
};

SelectInput.propTypes = {
  colors: PropTypes.arrayOf(PropTypes.object),
};

export default SelectInput;