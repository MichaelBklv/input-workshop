import './App.css';
import SelectInput from "./ws/component/SelectInput/SelectInput";
import SignIN from "./ws/component/SignIn/SignIn";

function App() {
  const colors = [
    {
      id: 1,
      value: '#FFFF00',
      name: 'Жёлтый'
    },
    {
      id: 2,
      value: '#008000',
      name: 'Зелёный'
    },
    {
      id: 3,
      value: '#FF0000',
      name: 'Красный'
    }
  ];
  return (
    <div className="App">
      <SelectInput colors={colors}></SelectInput>
      <SignIN></SignIN>
    </div>
  );
}

export default App;
